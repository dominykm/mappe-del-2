package ntnu.idatt2001.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CSVWriterTest {
    PatientList patientList;
    File file;
    File writeFile;
    Patient p1;
    Patient p2;
    @BeforeEach
    void initAll(){
        patientList = new PatientList();
        file = new File("src/main/resources/test.csv");
        writeFile = new File("src/main/resources/writeTest.csv");
        p1 = new Patient("12345678912", "Bob","The Builder","", "Josh");
        p2 = new Patient("78945612310","Greg", "Gregenov","","Steve Joan");
    }
    @Test
    @DisplayName("Test that reads from a test.csv file")
    void readFromFIle() {
        try{
            CSVWriter.readFromFIle(file,patientList);
        }catch (IOException e){
            System.out.println(e);
        }
        //checks if the patients from the file are in the patient list
        assertTrue(patientList.getPatientList().contains(p1));
        assertTrue(patientList.getPatientList().contains(p2));
    }

    @Test
    @DisplayName("Test that tries to write to a file")
    void writeToFile() {
        patientList.addPatient(new Patient("45621379810","name", "lastname","","Steve"));
        patientList.addPatient(new Patient("12378946514","name","lastname","","Steve"));
        try{
            CSVWriter.writeToFile(writeFile,patientList);
            BufferedReader reader = new BufferedReader(new FileReader(writeFile));
            //reads the line and checks if the line has expected information and correct formatting
            assertTrue(reader.readLine().equalsIgnoreCase("firstName;lastNam;generalPractitioner;socialSecurityNumber"));
            assertTrue(reader.readLine().equalsIgnoreCase("name;lastname;Steve;45621379810;"));
            assertTrue(reader.readLine().equalsIgnoreCase("name;lastname;Steve;12378946514;"));
        }catch (IOException e){
            System.out.println(e);
        }


    }
}