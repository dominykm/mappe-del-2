package ntnu.idatt2001.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientListTest {
    PatientList patientList;
    Patient p1;
    Patient p2;

    @BeforeEach
    void initAll(){
        patientList = new PatientList();
        p1 = new Patient("12345678912", "Bob","The Builder","Broken arm", "Josh");
        p2 = new Patient("12345678912","Fake Bob", "The Builder", "","Steve");

    }
    @Nested
    class addPatient{
        @Test
        @DisplayName("Adds a patient to the list that is not there from before")
        void addNotExistingPatient() {
            //adding a patient to the list
            patientList.addPatient(p1);
            //checks if the first patient is in the list
            assertTrue(patientList.getPatientList().contains(p1));
        }

        @Test
        @DisplayName("Adds a patient that is in the list from before")
        void addExistingPatient(){
            //adding the person to the list
            patientList.addPatient(p1);
            //adding the same patient, that should not work and should return false
            assertFalse(patientList.addPatient(p1));
            //try to add different patient, but with the same social security number, should return false
            assertFalse(patientList.addPatient(p2));
        }
    }

@Nested
class removePatient{
    @Test
    @DisplayName("Removes a patient from the list that is there from before")
    void removeExistingPatient() {
        //adds a patient to the list
        patientList.addPatient(p1);
        //removes the patient, should return true
        assertTrue(patientList.removePatient(p1));
        //checks if the patient was removed, should return false
        assertFalse(patientList.getPatientList().contains(p1));
    }

    @Test
    @DisplayName("Removes a patient from the list that is not there from before")
    void removeNotExistingPatient(){
        //try to remove a patient, should return false
        assertFalse(patientList.removePatient(p1));

    }
}

}