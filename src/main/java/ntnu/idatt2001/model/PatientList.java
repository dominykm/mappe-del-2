package ntnu.idatt2001.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class PatientList {
    private ArrayList<Patient> patientList;

    /**
     * constructor to create a patient list
     */
    public PatientList(){
        patientList = new ArrayList<>();
    }
    /**
     * method to get the patient list
     * @return the patient list
     */
    public ArrayList<Patient> getPatientList() {
        return patientList;
    }
    /**
     * method that returns patient list as an observableList
     * @return ObservableList of the patient list
     */
    public ObservableList<Patient> getObservablePatientList(){
;
        return FXCollections.observableList(patientList);
    }
    /**
     * method to add a patient to the patient list
     * checks if the patient is there from before, if not adds the patient to the list
     * @param patient patient to be added to the list
     * @return returnst true if the patient was added, false if not
     */
    public boolean addPatient(Patient patient){
        if(!patientList.contains(patient)){
            patientList.add(patient);
            return true;
        }
        return false;
    }
    /**
     * method to remove a patient from the patient list
     * @param patient patient to be removed from the list
     * @return returns true if patient was removed, false if not
     */
    public boolean removePatient(Patient patient){
        return patientList.remove(patient);
    }
}
