package ntnu.idatt2001.model;

import java.util.Objects;

public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor to create a a patient with given data
     * @param socialSecurityNumber the social security number of the patient
     * @param firstName first name of the patient
     * @param lastName last name of the patient
     * @param diagnosis diagnosis of the patient
     * @param generalPractitioner general practitioner of the patient
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner){
        if(firstName.isBlank() || lastName.isBlank()){
            throw new IllegalArgumentException("First or last name can not be blank");
        }if(socialSecurityNumber.isBlank() || socialSecurityNumber.trim().length() != 11){
            throw new IllegalArgumentException("The patient must have a social security number and it must be 11 digits");
        }
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }
    /**
     * method to get the social security number
     * @return social security number of the patient
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    /**
     * method to set a new social security number
     * @param socialSecurityNumber new social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    /**
     * method to get the first name
     * @return first name of the patient
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * method to set a new first name
     * @param firstName new first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * method to get the last name
     * @return last name of the patient
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * method to set a new last name
     * @param lastName new last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * method to get the diagnosis
     * @return diagnosis of the patient
     */
    public String getDiagnosis() {
        return diagnosis;
    }
    /**
     * method to set a new diagnosis
     * @param diagnosis new diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    /**
     * method to get the general practitioner
     * @return general practitioner of the patient
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }
    /**
     * method to set a new general practitioner
     * @param generalPractitioner new general practitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * method to check if two patient objects are the same, they are the same if:
     * social security number is the same
     * @param o the other object you want to compare with
     * @return true if the patients are the same, false if they are not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(getSocialSecurityNumber(), patient.getSocialSecurityNumber());
    }
    /**
     * method that creates a unique id for the patient object
     * @return the unique id for the object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getSocialSecurityNumber());
    }
}
