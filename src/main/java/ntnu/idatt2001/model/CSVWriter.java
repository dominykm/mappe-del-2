package ntnu.idatt2001.model;

import java.io.*;

/**
 * helper class that helps with writing and reading from CSV files
 */
public class CSVWriter {

    /**
     * method that reads from csv file and puts the information in a patient list
     * @param file file that is read from
     * @param patientList the old patient list
     * @return the new patient list with information from the file
     * @throws IOException throws exception if something is wrong while reading the file
     */
    public static PatientList readFromFIle(File file, PatientList patientList) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));

        patientList.getPatientList().clear();

        String line = "";
        reader.readLine();
        while((line = reader.readLine()) != null){
            String[] patientAsArray = line.split(";");
            patientList.addPatient(new Patient(patientAsArray[3],patientAsArray[0],patientAsArray[1],"",patientAsArray[2]));
        }
        return patientList;
    }

    /**
     * method that writes to a csv file from a patient list
     * @param file the file information is written to
     * @param patientList the patient list information is take from
     * @throws IOException throed exception if something is wrong while writing to a file
     */
    public static void writeToFile(File file, PatientList patientList) throws IOException{
        FileWriter writer = new FileWriter(file);
        writer.write("firstName;lastNam;generalPractitioner;socialSecurityNumber\n");

        for(Patient p : patientList.getPatientList()){
            writer.write(p.getFirstName() + ";" + p.getLastName() + ";" + p.getGeneralPractitioner() + ";" + p.getSocialSecurityNumber() + ";\n");
        }
        writer.close();
    }
}
