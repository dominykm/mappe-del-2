package ntnu.idatt2001.Gui;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * a class that creates and returns different Node objects
 */
public class FactoryGui {

    /**
     * method that returns node object that is chosen
     * @param nodeName the name of the node that the method will return
     * @return
     */
    public static Node create(String nodeName){
        if(nodeName.isBlank()){
            return null;
        }else if(nodeName.equalsIgnoreCase("button")){
            return new Button();
        }else if(nodeName.equalsIgnoreCase("textfield")){
            return new TextField();
        }else if(nodeName.equalsIgnoreCase("tableview")){
            return new TableView<>();
        }else if(nodeName.equalsIgnoreCase("menubar")){
            return new MenuBar();
        }else if(nodeName.equalsIgnoreCase("borderpane")){
            return new BorderPane();
        }else if(nodeName.equalsIgnoreCase("vbox")){
            return new VBox();
        }else if(nodeName.equalsIgnoreCase("gridpane")){
            return new GridPane();
        }else if(nodeName.equalsIgnoreCase("label")){
            return new Label();
        }else if(nodeName.equalsIgnoreCase("imageview")){
            return new ImageView();
        }
        return null;
    }
}
