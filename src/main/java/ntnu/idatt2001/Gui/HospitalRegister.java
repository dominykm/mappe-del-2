package ntnu.idatt2001.Gui;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ntnu.idatt2001.model.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;


public class HospitalRegister extends Application {
    PatientList patientList = new PatientList();
    TableView<Patient> patientTable;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Hospital Register ");

        patientTable = (TableView<Patient>) FactoryGui.create("tableview");
        TableColumn firstNameColumn = new TableColumn("First name");
        TableColumn lastNameColumn = new TableColumn("Last name");
        TableColumn socialNumbColumn = new TableColumn("Social security number");
        patientTable.getColumns().addAll(firstNameColumn,lastNameColumn,socialNumbColumn);

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Patient,String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Patient,String>("lastName"));
        socialNumbColumn.setCellValueFactory(new PropertyValueFactory<Patient,String>("socialSecurityNumber"));
        patientTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        update();

        Menu fileMenu = new Menu("file");
        MenuItem importItem = new MenuItem("Import from .CSV...");
        MenuItem exportItem = new MenuItem("Export to .CSV...");
        Menu editMenu = new Menu("edit");
        MenuItem addItem = new MenuItem("Add new patient...");
        MenuItem editItem = new MenuItem("Edit selected patient");
        MenuItem deleteItem = new MenuItem("Delete selected patient");
        Menu helpMenu = new Menu("help");
        MenuItem aboutItem = new MenuItem("About");

        fileMenu.getItems().addAll(importItem, exportItem);
        editMenu.getItems().addAll(addItem, editItem, deleteItem);
        helpMenu.getItems().addAll(aboutItem);

        MenuBar menuBar = (MenuBar) FactoryGui.create("menubar");
        menuBar.getMenus().addAll(fileMenu,editMenu,helpMenu);

        Button addButton = (Button) FactoryGui.create("button");
        ImageView addPatientImage = (ImageView) FactoryGui.create("imageView");
        addPatientImage.setImage(new Image("add-user.png",50,50,true,false));
        addButton.setGraphic(addPatientImage);
        addButton.setTooltip(new Tooltip("Add new Patient"));

        Button editButton = (Button) FactoryGui.create("button");
        ImageView editPatientImage = (ImageView) FactoryGui.create("imageview");
        editPatientImage.setImage(new Image("edit-user.png",50,50,true,false));
        editButton.setGraphic(editPatientImage);
        editButton.setTooltip(new Tooltip("Edit selected Patient"));

        Button removeButton = (Button) FactoryGui.create("button");
        ImageView removePatientImage = (ImageView) FactoryGui.create("imageview");
        removePatientImage.setImage(new Image("remove-user.png",50,50,true,false));
        removeButton.setGraphic(removePatientImage);
        removeButton.setTooltip(new Tooltip("Remove selected Patient"));

        addButton.setOnAction(actionEvent -> patientStage("add"));
        addItem.setOnAction(actionEvent -> patientStage("add"));

        editButton.setOnAction(actionEvent -> patientStage("edit"));
        editItem.setOnAction(actionEvent -> patientStage("edit"));

        removeButton.setOnAction(actionEvent -> handleDeletePerson());
        deleteItem.setOnAction(actionEvent -> handleDeletePerson());

        aboutItem.setOnAction(actionEvent -> handleHelpButton());

        importItem.setOnAction(actionEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose a csv file to open");
            File file = fileChooser.showOpenDialog(stage);
            if(handleWrongFile(file)){
                try{
                patientList = CSVWriter.readFromFIle(file,patientList);
                update();
                }catch (IOException e){
                    handleException(e.getMessage());
                }
            }
        });

        exportItem.setOnAction(actionEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose a csv file to export to");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
            File file = fileChooser.showSaveDialog(stage);
            try{
                CSVWriter.writeToFile(file,patientList);
                update();
            }catch (IOException e){
                handleException(e.getMessage());
            }
        });


        BorderPane pane = (BorderPane) FactoryGui.create("borderpane");
        pane.setCenter(patientTable);
        pane.setTop(menuBar);
        VBox buttonBox = (VBox) FactoryGui.create("vbox");
        buttonBox.setSpacing(10);
        buttonBox.getChildren().addAll(addButton,editButton,removeButton);
        pane.setLeft(buttonBox);
        Scene mainScene = new Scene(pane,600,600);
        stage.setScene(mainScene);
        stage.show();
    }

    /**
     * private method that creates view or add patient stage
     * @param mode chooses if the user selected add patient or edit patient
     */
    private void patientStage(String mode){
        Stage addPatientStage = new Stage();
        addPatientStage.setHeight(330);
        addPatientStage.setWidth(460);

        GridPane viewPatient = (GridPane) FactoryGui.create("gridpane");
        viewPatient.setHgap(20);
        viewPatient.setVgap(20);
        viewPatient.setPadding(new Insets(10,10,10,10));

        Label firstNameLabel = (Label) FactoryGui.create("label");
        firstNameLabel.setText("Fist name:");
        Label lastNameLabel = (Label) FactoryGui.create("label");
        lastNameLabel.setText("Last name:");
        Label socialNumbLabel = (Label) FactoryGui.create("label");
        socialNumbLabel.setText("Social security number:");
        Label practitionerLabel =(Label) FactoryGui.create("label");
        practitionerLabel.setText("General Practitioner");
        Label diagnosisLabel = (Label) FactoryGui.create("label");
        diagnosisLabel.setText("Diagnosis");


        TextField firstNameField = (TextField) FactoryGui.create("textfield");
        firstNameField.setPromptText("First Name");
        TextField lastNameField = (TextField) FactoryGui.create("textfield");
        lastNameField.setPromptText("Last name");
        TextField socialNumbField = (TextField) FactoryGui.create("textfield");
        socialNumbField.setPromptText("Social security number");
        TextField practitionerField = (TextField) FactoryGui.create("textfield");
        practitionerField.setPromptText("General practitioner");
        TextField diagnosisField = (TextField) FactoryGui.create("textfield");
        diagnosisField.setPromptText("Diagnosis of the patient");

        Button okButton = (Button) FactoryGui.create("button");
        okButton.setText("Ok");
        Button cancelButton = (Button) FactoryGui.create("button");
        cancelButton.setText("Cancel");
        Button deleteButton = (Button) FactoryGui.create("button");
        deleteButton.setText("Delete");

        if(mode.equals("add")){
            addPatientStage.setTitle("Patient details -Add");
            viewPatient.add(cancelButton,3,5);

                okButton.setOnAction(actionEvent -> {
                    if(validateInput(firstNameField.getText(),lastNameField.getText(),socialNumbField.getText())) {
                        patientList.addPatient(new Patient(socialNumbField.getText(), firstNameField.getText(), lastNameField.getText(), diagnosisField.getText(), practitionerField.getText()));
                        update();
                        addPatientStage.close();
                    }
                });
        }
        if(mode.equals("edit") && patientTable.getSelectionModel().getSelectedItem() != null){
            Patient editPatient = patientTable.getSelectionModel().getSelectedItem();
            firstNameField.setText(editPatient.getFirstName());
            lastNameField.setText(editPatient.getLastName());
            socialNumbField.setText(editPatient.getSocialSecurityNumber());
            practitionerField.setText(editPatient.getGeneralPractitioner());
            diagnosisField.setText(editPatient.getDiagnosis());

            addPatientStage.setTitle("Patient details -Edit");
            viewPatient.add(deleteButton,3,5);

                okButton.setOnAction(actionEvent -> {
                    if(validateInput(firstNameField.getText(),lastNameField.getText(),socialNumbField.getText())){
                    editPatient.setFirstName(firstNameField.getText());
                    editPatient.setLastName(lastNameField.getText());
                    editPatient.setSocialSecurityNumber(socialNumbField.getText());
                    editPatient.setGeneralPractitioner(practitionerField.getText());
                    editPatient.setDiagnosis(diagnosisField.getText());
                    addPatientStage.close();
                    update();
                    }
                });

            deleteButton.setOnAction(actionEvent -> {
                patientList.removePatient(editPatient);
                addPatientStage.close();
                update();
            });
        }

        cancelButton.setOnAction(actionEvent-> addPatientStage.close());

        viewPatient.add(firstNameLabel,0,0);
        viewPatient.add(firstNameField,1,0);
        viewPatient.add(lastNameLabel,0,1);
        viewPatient.add(lastNameField,1,1);
        viewPatient.add(socialNumbLabel,0,2);
        viewPatient.add(socialNumbField,1,2);
        viewPatient.add(practitionerLabel,0,3);
        viewPatient.add(practitionerField,1,3);
        viewPatient.add(diagnosisLabel,0,4);
        viewPatient.add(diagnosisField,1,4);
        viewPatient.add(okButton,2,5);

        if(mode.equals("add") || patientTable.getSelectionModel().getSelectedItem() != null){
            addPatientStage.setScene(new Scene(viewPatient));
            addPatientStage.show();
        }

    }

    /**
     * private method that creates the delete confirmation window when delete is selected
     */
    private void handleDeletePerson() {
        int selectedIndex = patientTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            Alert deleteConformation = new Alert(Alert.AlertType.CONFIRMATION);
            deleteConformation.setTitle("Delete Confirmation");
            deleteConformation.setHeaderText("Delete confirmation");
            deleteConformation.setContentText("Are you sure you want to delete this item?");
            Optional<ButtonType> deleteResult = deleteConformation.showAndWait();
            if (deleteResult.get() == ButtonType.OK) {
                patientTable.getItems().remove(selectedIndex);
            } else {
                deleteConformation.close();
            }
        }
    }

    /**
     * private method that creates info window when about is selected
     */
    private void handleHelpButton(){
        Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
        infoAlert.setTitle("Information Dialog - About");
        infoAlert.setHeaderText("Patient Register \n v0.1-SNAPSHOT");
        infoAlert.setContentText("Application created by \n Dominykas Mazys \n 02.05.2021");
        infoAlert.showAndWait();
    }

    /**
     * private method that checks user input and displays window if there is
     * it checks if the first and last name is blank
     * if the social security number is blank
     * if the social security number is 11 digits
     * @param firstName input of the first name
     * @param lastName input of the last name
     * @param socialSecurityNumber input of the social security number
     * @return returns true if there is no invalid input, false if there
     */
    private boolean validateInput(String firstName, String lastName, String socialSecurityNumber){
        Alert inputAlert = new Alert(Alert.AlertType.INFORMATION);
        String inputString = "";
        boolean input = true;
        inputAlert.setTitle("Input failure");
        inputAlert.setHeaderText("Some of the input was wrong");
        if(socialSecurityNumber.isBlank()){
            inputString += "The patient must have a social security number\n";
            input = false;
        }
        if(socialSecurityNumber.trim().length() != 11){
            inputString += "The social security number must be 11 digits\n";
            input = false;
        }
        if(firstName.isBlank() || lastName.isBlank()){
            inputString += "First or last name can not be blank\n";
            input = false;
        }
        inputAlert.setContentText(inputString);
        if(input == false){
            inputAlert.showAndWait();
        }

        return input;
    }

    /**
     * private method that creates a window when there is an unexpected error
     * @param e error message
     */
    private void handleException(String e){
        Alert failureAlert = new Alert(Alert.AlertType.INFORMATION);
        failureAlert.setTitle("Failure");
        failureAlert.setHeaderText("Failure");
        failureAlert.setContentText("Something went wrong, contact the personnel whit this error message:\n" + e);
    }

    /**
     * checks if the file is correct type
     * if the file is wrong type, an information alert will be created
     * @param file the file chosen
     * @return returns true if is correct type
     * returns false if not
     */
    private boolean handleWrongFile(File file){
        boolean correctFile = false;
        String extension = "";
        int index = file.getName().lastIndexOf('.');
        if (index > 0) {
            extension = file.getName().substring(index + 1);
        }
        if(extension.equalsIgnoreCase("csv")){
            correctFile = true;
        }else{
            Alert wrongFileAlert = new Alert(Alert.AlertType.INFORMATION);
            wrongFileAlert.setTitle("Warning");
            wrongFileAlert.setHeaderText("Wrong file type");
            wrongFileAlert.setContentText("You need to choose a file of csv type");
            wrongFileAlert.showAndWait();
        }
        return correctFile;
    }
    /**
     * private method that updates tableview
     */
    private void update(){
        patientTable.setItems(patientList.getObservablePatientList());
        patientTable.refresh();
    }
}
